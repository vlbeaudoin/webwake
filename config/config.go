package config

import (
	"codeberg.org/vlbeaudoin/serpents"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

type config struct {
	Web struct {
		MAC      string
		Password string
		Port     uint
		Tool     string
		Username string
	}
}

func Config() (cfg *config, err error) {
	return cfg, viper.Unmarshal(&cfg)
}

func Register(f *pflag.FlagSet) error {
	return nil
}

func RegisterWeb(f *pflag.FlagSet) error {
	if err := serpents.StringP(f,
		"web.mac", "web-mac", "m", "",
		"MAC address on which to attempt wake-on-lan"); err != nil {
		return err
	}

	if err := serpents.String(f,
		"web.password", "web-password", "webwake",
		"Password for the webwake instance"); err != nil {
		return err
	}

	if err := serpents.UintP(f,
		"web.port", "web-port", "p", 8080,
		"Port for the webwake instance"); err != nil {
		return err
	}

	if err := serpents.StringP(f,
		"web.tool", "web-tool", "t", "wol",
		"Executable to run for the wake-on-lan request { awake | wol }"); err != nil {
		return err
	}

	if err := serpents.String(f,
		"web.username", "web-username", "webwake",
		"Username for the webwake instance"); err != nil {
		return err
	}

	return nil
}
