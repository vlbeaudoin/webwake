/*
Copyright © 2023 Victor Lacasse-Beaudoin <victor.lacassebeaudoin@proton.me>
*/
package cmd

import (
	"fmt"
	"log"
	"net"

	"codeberg.org/vlbeaudoin/webwake/wake"
	"github.com/spf13/cobra"
)

// wolCmd represents the wol command
var wolCmd = &cobra.Command{
	Use:   "wol MAC",
	Args:  cobra.ExactArgs(1),
	Short: "Execute `wol` locally to wake a computer",
	Run: func(cmd *cobra.Command, args []string) {
		mac, err := net.ParseMAC(args[0])
		if err != nil {
			log.Fatal(err)
		}

		stdout, err := wake.Wol(mac)
		if err != nil {
			log.Fatal(err, " got: ", string(stdout))
		}

		fmt.Println(string(stdout))
	},
}

func init() {
	rootCmd.AddCommand(wolCmd)
}
