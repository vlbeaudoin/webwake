/*
Copyright © 2023 Victor Lacasse-Beaudoin <victor.lacassebeaudoin@proton.me>
*/
package cmd

import (
	"fmt"
	"log"
	"net"

	"codeberg.org/vlbeaudoin/webwake/wake"
	"github.com/spf13/cobra"
)

// awakeCmd represents the awake command
var awakeCmd = &cobra.Command{
	Use:   "awake MAC",
	Args:  cobra.ExactArgs(1),
	Short: "Execute `awake` locally to wake a computer",
	Run: func(cmd *cobra.Command, args []string) {
		mac, err := net.ParseMAC(args[0])
		if err != nil {
			log.Fatal(err)
		}

		stdout, err := wake.Awake(mac)
		if err != nil {
			log.Fatal(err, " got: ", string(stdout))
		}

		fmt.Println(string(stdout))
	},
}

func init() {
	rootCmd.AddCommand(awakeCmd)
}
