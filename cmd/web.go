/*
Copyright © 2023 Victor Lacasse-Beaudoin <victor.lacassebeaudoin@proton.me>
*/
package cmd

import (
	"crypto/subtle"
	"fmt"
	"log"
	"net"
	"net/http"

	"codeberg.org/vlbeaudoin/webwake/config"
	"codeberg.org/vlbeaudoin/webwake/wake"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/spf13/cobra"
)

// webCmd represents the web command
var webCmd = &cobra.Command{
	Use:   "web",
	Short: "Start the webwake webserver",
	Run: func(cmd *cobra.Command, args []string) {
		cfg, err := config.Config()
		if err != nil {
			log.Fatal(err)
		}

		mac, err := net.ParseMAC(cfg.Web.MAC)
		if err != nil {
			log.Fatal(err)
		}

		e := echo.New()

		//TODO add trailing slash
		e.Pre(middleware.AddTrailingSlash())

		e.Use(middleware.BasicAuth(
			func(username, password string, c echo.Context) (bool, error) {
				return (subtle.ConstantTimeCompare([]byte(username), []byte(cfg.Web.Username)) == 1 &&
						subtle.ConstantTimeCompare([]byte(password), []byte(cfg.Web.Password)) == 1),
					nil
			},
		))

		//TODO templating

		e.GET("/", func(c echo.Context) error {
			return c.HTML(http.StatusOK, fmt.Sprintf(`
  <h2>webwake</h1>
  <h3>MAC: %s</h3>
  <form action="/wake/%s/%s" method="post">
    <button name="wake" value="wake">wake</button>
  </form>
		  `, mac.String(), cfg.Web.Tool, mac.String()))
		})

		e.POST("/wake/:tool/:mac/", func(c echo.Context) (err error) {
			mac, err := net.ParseMAC(c.Param("mac"))
			if err != nil {
				return c.String(http.StatusOK, err.Error())
			}

			var result []byte

			switch tool := c.Param("tool"); tool {
			case "":
				return c.String(http.StatusOK, "webwake needs a wake-on-lan -t,--web-tool to run")
			case "awake":
				result, err = wake.Awake(mac)
			case "wol":
				result, err = wake.Wol(mac)
			default:
				return c.String(http.StatusOK, fmt.Sprintf("webwake cannot use unknown tool '%s'", tool))
			}

			if err != nil {
				return c.String(http.StatusOK, err.Error())
			}

			return c.String(http.StatusOK, string(result))
		})

		e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", cfg.Web.Port)))
	},
}

func init() {
	rootCmd.AddCommand(webCmd)

	if err := config.RegisterWeb(webCmd.Flags()); err != nil {
		log.Fatal(err)
	}
}
