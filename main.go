/*
Copyright © 2023 Victor Lacasse-Beaudoin <victor.lacassebeaudoin@proton.me>

*/
package main

import "codeberg.org/vlbeaudoin/webwake/cmd"

func main() {
	cmd.Execute()
}
