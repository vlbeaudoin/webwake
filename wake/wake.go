// Package wake provides functions to call wake-on-lan clients on the host machine
package wake

import (
	"net"
	"os/exec"
)

/*
Awake uses `awake` to wake a locally-connected machine with WakeOnLan enabled

`awake` must be installed on the server for this to work.
*/
func Awake(mac net.HardwareAddr) ([]byte, error) {
	return exec.Command("awake", mac.String()).CombinedOutput()
}

/*
Wol uses `wol` to wake a locally-connected machine with WakeOnLan enabled

`wol` must be installed on the server for this to work.
*/
func Wol(mac net.HardwareAddr) ([]byte, error) {
	return exec.Command("wol", mac.String()).CombinedOutput()
}
